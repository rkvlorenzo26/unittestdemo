package dev.rkvl.unittestdemo.dao;

import java.util.List;

import dev.rkvl.unittestdemo.entity.Customer;

public interface QueueDao {
    boolean addCustomer(Customer customer);

    boolean updateCustomerStatus(Integer id, String status);

    List<Customer> getOnQueueCustomers();

    List<Customer> getAllRecords();
}
