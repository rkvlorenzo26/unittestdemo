package dev.rkvl.unittestdemo.adapters;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import dev.rkvl.unittestdemo.R;
import dev.rkvl.unittestdemo.entity.Customer;
import dev.rkvl.unittestdemo.utils.IconGenerator;


public class QueueAdapter extends RecyclerView.Adapter<QueueAdapter.MyViewHolder> {

    private Context mContext;
    private List<Customer> customersList;

    public QueueAdapter(Context mContext, List<Customer> customersList) {
        this.mContext = mContext;
        this.customersList = customersList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView txtName;
        public TextView txtRemarks;
        public TextView txtDate;

        public MyViewHolder (View view) {
            super(view);

            imageView = view.findViewById(R.id.imageView);
            txtName = view.findViewById(R.id.txtName);
            txtRemarks = view.findViewById(R.id.txtRemarks);
            txtDate = view.findViewById(R.id.txtDate);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.queue_list_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.imageView.setImageDrawable(new IconGenerator().generate(String.valueOf(position + 1)));
        holder.txtName.setText(customersList.get(position).getName());
        holder.txtRemarks.setText(customersList.get(position).getRemarks());
        holder.txtDate.setText(customersList.get(position).getStartDate());
    }

    @Override
    public int getItemCount() {
        return customersList.size();
    }
}
