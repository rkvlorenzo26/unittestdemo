package dev.rkvl.unittestdemo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dev.rkvl.unittestdemo.entity.Customer;
import dev.rkvl.unittestdemo.utils.Common;
import dev.rkvl.unittestdemo.utils.Constants;

public class DBHelper extends SQLiteOpenHelper  {

    private static final String DATABASE_NAME = "CustomerDatabase";
    private static final int DATABASE_VERSION = 4;
    private static final String TABLE_NAME = "customers";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_REMARKS = "remarks";
    private static final String COLUMN_START_DATE= "start_date";
    private static final String COLUMN_END_DATE = "end_date";
    private static final String COLUMN_STATUS = "status";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_NAME + " TEXT NOT NULL,"
                + COLUMN_REMARKS + " TEXT NOT NULL,"
                + COLUMN_START_DATE + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,"
                + COLUMN_END_DATE + " DATETIME,"
                + COLUMN_STATUS + " TEXT NOT NULL"+ ")";
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
        sqLiteDatabase.execSQL(sql);
        onCreate(sqLiteDatabase);
    }

    public boolean addCustomer(Customer customer) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, customer.getName());
        contentValues.put(COLUMN_REMARKS, customer.getRemarks());
        contentValues.put(COLUMN_STATUS, customer.getStatus());
        contentValues.put(COLUMN_START_DATE, new Common().getCurrentDatetime());
        return db.insert(TABLE_NAME, null, contentValues) != -1;
    }

    public boolean updateStatus(Integer id, String status) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_STATUS, status);

        SQLiteDatabase db = this.getWritableDatabase();
        return db.update(TABLE_NAME, contentValues, COLUMN_ID + " =  ?", new String[]{String.valueOf(id)}) == 1;
    }

    public List<Customer> getCurrentQueue(String currentDate) {
        List<Customer> customersList = new ArrayList<>();

        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM ").append(TABLE_NAME).append(" WHERE ");
        query.append(COLUMN_START_DATE).append(" = ").append("'").append(currentDate).append("'");
        query.append(" AND ").append(COLUMN_STATUS).append(" = '").append(Constants.PENDING).append("';");

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query.toString(), null);

        while (cursor.moveToNext()) {
            Customer customer = new Customer();
            customer.setId(Integer.parseInt(cursor.getString(0)));
            customer.setName(cursor.getString(1));
            customer.setRemarks(cursor.getString(2));
            customer.setStartDate(cursor.getString(3));
            customer.setEndDate(cursor.getString(4));
            customer.setStatus(cursor.getString(5));
            customersList.add(customer);
        }

        return customersList;
    }

    public List<Customer> getAllRecords() {
        List<Customer> customersList = new ArrayList<>();

        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM ").append(TABLE_NAME).append(" ORDER BY ") .append(COLUMN_START_DATE).append(" DESC");

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query.toString(),null);

        while (cursor.moveToNext()) {
            Customer customer = new Customer();
            customer.setId(Integer.parseInt(cursor.getString(0)));
            customer.setName(cursor.getString(1));
            customer.setRemarks(cursor.getString(2));
            customer.setStartDate(cursor.getString(3));
            customer.setEndDate(cursor.getString(4));
            customer.setStatus(cursor.getString(5));
            customersList.add(customer);
        }

        return customersList;
    }
}
