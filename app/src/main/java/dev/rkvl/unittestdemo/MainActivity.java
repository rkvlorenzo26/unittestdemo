package dev.rkvl.unittestdemo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import dev.rkvl.unittestdemo.adapters.QueueAdapter;
import dev.rkvl.unittestdemo.database.DBHelper;
import dev.rkvl.unittestdemo.entity.Customer;
import dev.rkvl.unittestdemo.service.QueueService;
import dev.rkvl.unittestdemo.utils.Constants;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    private QueueAdapter queueAdapter;
    private List<Customer> customersList = new ArrayList<>();

    private DBHelper dbHelper;
    private QueueService queueService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DBHelper(this);
        queueService = new QueueService(this, dbHelper);

        recyclerView = findViewById(R.id.rvQueue);
        queueAdapter = new QueueAdapter(this, customersList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(queueAdapter);

        loadData();
    }

    private void loadData() {
        customersList.clear();
        customersList.addAll(queueService.getOnQueueCustomers());
        queueAdapter = new QueueAdapter(this, customersList);
        queueAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_new:
                generateNewCustomerDialog();
                break;
            // action with ID action_settings was selected
            case R.id.action_history:
                Toast.makeText(this, "Settings selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            default:
                break;
        }

        return true;
    }

    private void generateNewCustomerDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View dialogView = layoutInflater.inflate(R.layout.form_add_customer, null);
        final AlertDialog dialogForm = new AlertDialog.Builder(this).create();
        dialogForm.setView(dialogView);

        final TextView name = dialogView.findViewById(R.id.name);
        final TextView remarks = dialogView.findViewById(R.id.remarks);

        dialogView.findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Customer customer = new Customer(0, name.getText().toString(), remarks.getText().toString(), "", "", Constants.PENDING.toString());
                if (queueService.addCustomer(customer)) {
                    loadData();
                    dialogForm.hide();
                } else {
                    Toast.makeText(getBaseContext(), "Please fill up all fields.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialogForm.show();
    }
}
