package dev.rkvl.unittestdemo.service;

import android.content.Context;

import java.util.List;

import dev.rkvl.unittestdemo.dao.QueueDao;
import dev.rkvl.unittestdemo.database.DBHelper;
import dev.rkvl.unittestdemo.entity.Customer;
import dev.rkvl.unittestdemo.utils.Common;

public class QueueService implements QueueDao {

    private Context mContext;
    private DBHelper db;


    public QueueService(Context mContext, DBHelper db) {
        this.mContext = mContext;
        this.db = db;
    }

    @Override
    public boolean addCustomer(Customer customer) {
        if (customer == null) {
            return false;
        } else if (customer.getName().trim().isEmpty() || customer.getRemarks().trim().isEmpty()) {
            return false;
        } else {
            return db.addCustomer(customer);
        }
    }

    @Override
    public boolean updateCustomerStatus(Integer id, String status) {
        if (id == null || status == null || status.isEmpty()) {
            return false;
        } else {
         return db.updateStatus(id, status);
        }
    }

    @Override
    public List<Customer> getOnQueueCustomers() {
        return db.getCurrentQueue(new Common().getCurrentDate());
    }

    @Override
    public List<Customer> getAllRecords() {
        return db.getAllRecords();
    }
}
