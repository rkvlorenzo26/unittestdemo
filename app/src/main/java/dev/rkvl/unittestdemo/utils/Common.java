package dev.rkvl.unittestdemo.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class Common {

    /*
    * Get current date format yyyy-mm-dd
    * */
    public String getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(cal.getTime());
    }


    /*
     * Get current date format yyyy-mm-dd hh:mm:ss
     * */
    public String getCurrentDatetime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Manila"));
        return sdf.format(cal.getTime());
    }
}
