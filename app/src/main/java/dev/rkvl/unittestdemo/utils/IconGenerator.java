package dev.rkvl.unittestdemo.utils;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

public class IconGenerator {
    public TextDrawable generate(String number) {
        int color = ColorGenerator.MATERIAL.getColor(number);
        return TextDrawable.builder()
                .buildRound(number, color);
    }
}
