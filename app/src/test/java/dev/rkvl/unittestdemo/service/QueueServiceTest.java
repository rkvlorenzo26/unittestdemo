package dev.rkvl.unittestdemo.service;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import dev.rkvl.unittestdemo.database.DBHelper;
import dev.rkvl.unittestdemo.entity.Customer;
import dev.rkvl.unittestdemo.utils.Common;
import dev.rkvl.unittestdemo.utils.Constants;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class QueueServiceTest {

    private QueueService queueService;
    private Context mContextMock;
    private DBHelper dbHelperMock;

    @Before
    public void setUp() throws Exception {
        mContextMock = mock(Context.class);
        dbHelperMock = mock(DBHelper.class);
        queueService = new QueueService(mContextMock, dbHelperMock);
    }

    @Test
    public void addCustomer_emptyForm_returnedFalse() {
        Customer customer = new Customer(1, "ralph", "", "", "", Constants.COMPLETED.toString());
        when(dbHelperMock.addCustomer(customer)).thenReturn(false);
        assertFalse(queueService.addCustomer(customer));
    }

    @Test
    public void addCustomer_completeForm_returnedTrue() {
        Customer customer = new Customer(1, "ralph", "item return", "", "", Constants.COMPLETED.toString());
        when(dbHelperMock.addCustomer(customer)).thenReturn(true);
        assertTrue(queueService.addCustomer(customer));
    }

    @Test
    public void addCustomer_nullCustomer_returnedFalse() {
        assertFalse(queueService.addCustomer(null));
    }

    @Test
    public void updateCustomerStatus_nullId_returnedFalse() {
        assertFalse(queueService.updateCustomerStatus(null, Constants.COMPLETED.toString()));
    }

    @Test
    public void updateCustomerStatus_nullStatus_returnedFalse() {
        assertFalse(queueService.updateCustomerStatus(1, null));
    }

    @Test
    public void updateCustomer_correctParameters_returnedTrue() {
        when(dbHelperMock.updateStatus(1, Constants.COMPLETED.toString())).thenReturn(true);
        assertTrue(queueService.updateCustomerStatus(1, Constants.COMPLETED.toString()));

    }

    @Test
    public void updatCustomerStatus_emptyStatus_returnedFalse() {
        assertFalse(queueService.updateCustomerStatus(1, ""));
    }

    @Test
    public void getOnQueueCustomers_dummyList_returnedDummyList() {
        List<Customer> customerList = new ArrayList<>();
        customerList.add(new Customer(1, "John", "Item Repair", new Common().getCurrentDatetime(), new Common().getCurrentDatetime(), Constants.COMPLETED.toString()));
        customerList.add(new Customer(1, "John", "Item Repair", new Common().getCurrentDatetime(), new Common().getCurrentDatetime(), Constants.COMPLETED.toString()));
        customerList.add(new Customer(1, "John", "Item Repair", new Common().getCurrentDatetime(), new Common().getCurrentDatetime(), Constants.COMPLETED.toString()));
        when(dbHelperMock.getCurrentQueue(new Common().getCurrentDate())).thenReturn(customerList);
        assertEquals(customerList.size(), queueService.getOnQueueCustomers().size());
    }

    @Test
    public void getOnQueueCustomers_emptyList_returnedEmptyList() {
        List<Customer> customerList = new ArrayList<>();
        when(dbHelperMock.getCurrentQueue(new Common().getCurrentDate())).thenReturn(customerList);
        assertEquals(customerList.size(), queueService.getOnQueueCustomers().size());
    }

    @Test
    public void getAllRecords_dummyList_returnedDummyList() {
        List<Customer> customerList = new ArrayList<>();
        customerList.add(new Customer(1, "John", "Item Repair", new Common().getCurrentDatetime(), new Common().getCurrentDatetime(), Constants.COMPLETED.toString()));
        customerList.add(new Customer(1, "John", "Item Repair", new Common().getCurrentDatetime(), new Common().getCurrentDatetime(), Constants.COMPLETED.toString()));
        customerList.add(new Customer(1, "John", "Item Repair", new Common().getCurrentDatetime(), new Common().getCurrentDatetime(), Constants.COMPLETED.toString()));
        when(dbHelperMock.getAllRecords()).thenReturn(customerList);
        assertEquals(customerList.size(), queueService.getAllRecords().size());
    }

    @Test
    public void getAllRecords_emptyList_returnedEmptyList() {
        List<Customer> customerList = new ArrayList<>();
        when(dbHelperMock.getAllRecords()).thenReturn(customerList);
        assertEquals(customerList.size(), queueService.getAllRecords().size());
    }



}